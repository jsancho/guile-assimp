;;; guile-assimp, foreign interface to libassimp
;;; Copyright (C) 2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


(define-module (assimp low-level postprocess)
  #:use-module (assimp low-level)
  #:export (ai-process-steps
	    ai-process-convert-to-left-handed
	    ai-process-preset-target-realtime-fast
	    ai-process-preset-target-realtime-quality
	    ai-process-preset-target-realtime-max-quality))


(define-bitfield
  ai-process-steps
  (calc-tangent-space #x1)
  (join-identical-vertices #x2)
  (make-left-handed #x4)
  (triangulate #x8)
  (remove-component #x10)
  (gen-normals #x20)
  (gen-smooth-normals #x40)
  (split-large-meshes #x80)
  (pretransform-vertices #x100)
  (limit-bone-weights #x200)
  (validate-data-structure #x400)
  (improve-cache-locality #x800)
  (remove-redundant-materials #x1000)
  (fix-infacing-normals #x2000)
  (sort-by-ptype #x8000)
  (find-degenerates #x10000)
  (find-invalid-data #x20000)
  (gen-UV-coords #x40000)
  (transform-UV-coords #x80000)
  (find-instances #x100000)
  (optimize-meshes #x200000)
  (optimize-graph #x400000)
  (flip-UVs #x800000)
  (flip-winding-order #x1000000)
  (split-by-bone-count #x2000000)
  (debone #x4000000))

(define ai-process-convert-to-left-handed
  (ai-process-steps
   make-left-handed
   flip-UVs
   flip-winding-order))

(define ai-process-preset-target-realtime-fast
  (ai-process-steps
   calc-tangent-space
   gen-normals
   join-identical-vertices
   triangulate
   gen-UV-coords
   sort-by-ptype))

(define ai-process-preset-target-realtime-quality
  (ai-process-steps
   calc-tangent-space
   gen-smooth-normals
   join-identical-vertices
   improve-cache-locality
   limit-bone-weights
   remove-redundant-materials
   split-large-meshes
   triangulate
   gen-UV-coords
   sort-by-ptype
   find-degenerates
   find-invalid-data))

(define ai-process-preset-target-realtime-max-quality
  (+ ai-process-preset-target-realtime-quality
     (ai-process-steps
      find-instances
      validate-data-structure
      optimize-meshes
      debone)))
