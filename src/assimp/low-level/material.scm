;;; guile-assimp, foreign interface to libassimp
;;; Copyright (C) 2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


(define-module (assimp low-level material)
  #:use-module (assimp low-level)
  #:use-module (assimp low-level types)
  #:use-module (system foreign)
  #:export (parse-aiMaterial
	    parse-aiMaterialProperty
	    ai-material-key
	    aiGetMaterialColor
	    aiGetMaterialFloatArray
	    aiGetMaterialIntegerArray))


(define-struct-parser parse-aiMaterialProperty
  (mKey aiString-type)
  (mSemantic unsigned-int)
  (mIndex unsigned-int)
  (mDataLength unsigned-int)
  (mType unsigned-int)
  (mData '*))

(define-struct-parser parse-aiMaterial
  (mProperties '*)
  (mNumProperties unsigned-int)
  (mNumAllocated unsigned-int))


(define-enumeration
  ai-material-key
  (name '("?mat.name" 0 0))
  (twosided '("$mat.twosided" 0 0))
  (shading-model '("$mat.shadingm" 0 0))
  (enable-wireframe '("$mat.wireframe" 0 0))
  (blend-func '("$mat.blend" 0 0))
  (opacity '("$mat.opacity" 0 0))
  (bumpscaling '("$mat.bumpscaling" 0 0))
  (shininess '("$mat.shininess" 0 0))
  (reflectivity '("$mat.reflectivity" 0 0))
  (shininess-strength '("$mat.shinpercent" 0 0))
  (refracti '("$mat.refracti" 0 0))
  (color-diffuse '("$clr.diffuse" 0 0))
  (color-ambient '("$clr.ambient" 0 0))
  (color-specular '("$clr.specular" 0 0))
  (color-emissive '("$clr.emissive" 0 0))
  (color-transparent '("$clr.transparent" 0 0))
  (color-reflective '("$clr.reflective" 0 0))
  (global-background-image '("?bg.global" 0 0)))


(define-assimp-function (aiGetMaterialColor '* '* unsigned-int unsigned-int '*) -> int)
(define-assimp-function (aiGetMaterialFloatArray '* '* unsigned-int unsigned-int '* '*) -> int)
(define-assimp-function (aiGetMaterialIntegerArray '* '* unsigned-int unsigned-int '* '*) -> int)
