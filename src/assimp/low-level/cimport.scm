;;; guile-assimp, foreign interface to libassimp
;;; Copyright (C) 2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


(define-module (assimp low-level cimport)
  #:use-module (assimp low-level)
  #:use-module (system foreign)
  #:export (aiImportFile
	    aiReleaseImport
	    aiGetPredefinedLogStream
	    aiAttachLogStream
	    aiDetachAllLogStreams
	    aiTransformVecByMatrix4
	    aiMultiplyMatrix3
	    aiMultiplyMatrix4
	    aiIdentityMatrix3
	    aiIdentityMatrix4
	    aiTransposeMatrix3
	    aiTransposeMatrix4))

(define-assimp-function (aiImportFile '* unsigned-int) -> '*)
(define-assimp-function (aiReleaseImport '*) -> void)
(define-assimp-function (aiGetPredefinedLogStream unsigned-int '*) -> (list '* '* '*))
(define-assimp-function (aiAttachLogStream '*) -> void)
(define-assimp-function (aiDetachAllLogStreams) -> void)

(define-assimp-function (aiTransformVecByMatrix4 '* '*) -> void)
(define-assimp-function (aiMultiplyMatrix3 '* '*) -> void)
(define-assimp-function (aiMultiplyMatrix4 '* '*) -> void)
(define-assimp-function (aiIdentityMatrix3 '*) -> void)
(define-assimp-function (aiIdentityMatrix4 '*) -> void)
(define-assimp-function (aiTransposeMatrix3 '*) -> void)
(define-assimp-function (aiTransposeMatrix4 '*) -> void)
