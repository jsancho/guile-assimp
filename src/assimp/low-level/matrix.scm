;;; guile-assimp, foreign interface to libassimp
;;; Copyright (C) 2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


(define-module (assimp low-level matrix)
  #:use-module (assimp low-level)
  #:use-module (system foreign))


(define-struct-parser parse-aiMatrix3x3
  (a1 float)
  (a2 float)
  (a3 float)
  (b1 float)
  (b2 float)
  (b3 float)
  (c1 float)
  (c2 float)
  (c3 float))

(define-struct-parser parse-aiMatrix4x4
  (a1 float)
  (a2 float)
  (a3 float)
  (a4 float)
  (b1 float)
  (b2 float)
  (b3 float)
  (b4 float)
  (c1 float)
  (c2 float)
  (c3 float)
  (c4 float)
  (d1 float)
  (d2 float)
  (d3 float)
  (d4 float))

(export parse-aiMatrix3x3
	parse-aiMatrix4x4)
