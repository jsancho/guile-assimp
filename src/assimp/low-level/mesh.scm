;;; guile-assimp, foreign interface to libassimp
;;; Copyright (C) 2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


(define-module (assimp low-level mesh)
  #:use-module (assimp low-level)
  #:use-module (assimp low-level types)
  #:use-module (system foreign))


(define-struct-parser parse-aiFace
  (mNumIndices unsigned-int)
  (mIndices '*))

(export parse-aiFace)


(define-struct-parser parser-aiVertexWeight
  (mVertexId unsigned-int)
  (mWeight float))

(export parse-aiVertexWeight)


(define-struct-parser parse-aiBone
  (mName aiString-type)
  (mNumWeights unsigned-int)
  (mWeights '*)
  (mOffsetMatrix aiMatrix4x4-type))

(export parse-aiBone)


(define AI_MAX_NUMBER_OF_COLOR_SETS #x8)
(define AI_MAX_NUMBER_OF_TEXTURECOORDS #x8)

(define-struct-parser parse-aiMesh
  (mPrimitiveTypes unsigned-int)
  (mNumVertices unsigned-int)
  (mNumFaces unsigned-int)
  (mVertices '*)
  (mNormals '*)
  (mTangents '*)
  (mBitangents '*)
  (mColors (make-list AI_MAX_NUMBER_OF_COLOR_SETS '*))
  (mTextureCoords (make-list AI_MAX_NUMBER_OF_TEXTURECOORDS '*))
  (mNumUVComponents (make-list AI_MAX_NUMBER_OF_TEXTURECOORDS unsigned-int))
  (mFaces '*)
  (mNumBones unsigned-int)
  (mBones '*)
  (mMaterialIndex unsigned-int)
  (mName aiString-type)
  (mNumAnimMeshes unsigned-int)
  (mAnimMeshes '*))

(export parse-aiMesh)
