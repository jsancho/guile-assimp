;;; guile-assimp, foreign interface to libassimp
;;; Copyright (C) 2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


(define-module (assimp low-level types)
  #:use-module (assimp low-level)
  #:use-module (system foreign)
  #:export (aiString-type
	    aiMatrix4x4-type
	    ai-default-log-stream))


(define aiString-type
  (list size_t (make-list 1024 int8)))

(define aiMatrix4x4-type
  (make-list 16 float))

(define-enumeration
  ai-default-log-stream
  (file #x1)
  (stdout #x2)
  (stderr #x4)
  (debugger #x8)
  (ai-dls-enforce-enum-size #x7fffffff))
