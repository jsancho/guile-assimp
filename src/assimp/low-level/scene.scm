;;; guile-assimp, foreign interface to libassimp
;;; Copyright (C) 2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


(define-module (assimp low-level scene)
  #:use-module (assimp low-level)
  #:use-module (assimp low-level types)
  #:use-module (system foreign))


(define-struct-parser parse-aiNode
  (mName aiString-type)
  (mTransformation aiMatrix4x4-type)
  (mParent '*)
  (mNumChildren unsigned-int)
  (mChildren '*)
  (mNumMeshes unsigned-int)
  (mMeshes '*))

(export parse-aiNode)


(define-struct-parser parse-aiScene
  (mFlags unsigned-int)
  (mRootNode '*)
  (mNumMeshes unsigned-int)
  (mMeshes '*)
  (mNumMaterials unsigned-int)
  (mMaterials '*)
  (mNumAnimations unsigned-int)
  (mAnimations '*)
  (mNumTextures unsigned-int)
  (mTextures '*)
  (mNumLights unsigned-int)
  (mLights '*)
  (mNumCameras unsigned-int)
  (mCameras '*)
  (mPrivate '*))

(export parse-aiScene)
